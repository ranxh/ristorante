package com.company.ristorante.persone;

import java.util.Date;

public class Cliente implements Persona {

    private String nome;
    private String cognome;
    private Date dataDiNascita;
    private GenderKind sesso;

    /**
     * Costruttore
     */
    Cliente(String n, String c, Date d){
        this.nome = n;
        this.cognome = c;
        this.dataDiNascita = d;
    }

    public void effetuaOrdine(){}

    public void effetuaPrenotazione() {
//        Prenotazione p = new Prenotazione();
    }

    public void richiedeMenu(){
    }

    public String getNome(){
        return this.nome;
    }

    public String getCognome() {
        return this.nome;
    }

    public Date getDataDiNascita(){
        return this.dataDiNascita;
    }

     @Override
    public GenderKind getGenderKind() {
        return this.sesso;
    }
}
