package com.company.ristorante.persone;

import java.util.Date;

public interface Persona {

    public String getNome();
    public String getCognome();
    public Date getDataDiNascita();
    public Enum getGenderKind();
}
