package com.company.ristorante.persone;

public class AggiungiPersonale {
     public static void main(String a[]){
        /**
         * Uso la factory per nascondere i dettagli relativi alla costruzione
         * degli oggeti; nel nostro caso per il personale
         */
        PersonaleFactory theFactory = new PersonaleFactory();
        Persona p = theFactory.createDipendente("cassiere");
        System.out.println("Cassiere:");
        System.out.format("nome: %s\n",p.getNome());
        System.out.format("sesso: %s\n",p.getGenderKind());
    }
}
