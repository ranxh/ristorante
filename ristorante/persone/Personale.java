package com.company.ristorante.persone;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Vector;

/**
 * MISSIONE
 * Questa classe provvede un ADT per il personale
 *
 */
public class Personale {

    private Vector<Persona> dipendenti;

    /**
     * costruttore
     * EFFETTO; initializza a a un nuovo personale, vuoto
     */
    Personale(){
        this.dipendenti = new Vector<Persona>();
    }


    /**
     * MODIFICA il vettore dipendenti
     * @param p la persona da aggiungere al personale
     * @return True se l'operazione va a buon fine
     */
    public boolean addPersonale(Persona p){
        this.dipendenti.add(p);
        return true; //TODO
    }


    /** ====================================
     * CLASSE INTERNA per l'iteratore
     */
    private class PersonaleIterator implements Iterator<Persona> {

        /**
         * INVARIANTE
         * quando questo iteratore viene creato dipendenti tiene la coppia dei
         * dipendinti, current e un integer i cosi che listaPersonale[i] e il primo
         * dipendente non ancora tornato
         * current==dipendenti.size() abbiamo esplorato tutta la lista
         */

        private int current;
        final private Vector<Persona> dipendenti;

        PersonaleIterator(Personale p){
            this.dipendenti = (Vector<Persona>) p.dipendenti.clone();
            this.current = 0;
        }

        @Override
        public boolean hasNext(){
            return this.current < dipendenti.size();
        }

        @Override
        public Persona next(){
            if (this.current < dipendenti.size()){
                Persona res = this.dipendenti.get(this.current);
                this.current++;
                return(res);
            }
            throw new NoSuchElementException("Went beyond the available values");
        }
    }

}
