package com.company.ristorante.persone;

import java.util.Date;

public class Cameriere implements Persona {

    private String nome;
    private String cognome;
    private Date dataDiNascita;
    private boolean disponibile;
    private GenderKind sesso;

    /**
     * Costruttore
     */
    Cameriere(String n, String c, Date d){
        this.nome = n;
        this.cognome = c;
        this.dataDiNascita = d;
        this.disponibile = true;
    }

    public void gestisce(){}

    public String getNome(){
        return this.nome;
    }

    public String getCognome() {
        return this.nome;
    }

    public Date getDataDiNascita(){
        return this.dataDiNascita;
    }

    @Override
    public GenderKind getGenderKind() {
        return this.sesso;
    }
}
