package com.company.ristorante.persone;

public class PersonaleFactory {
    public Persona createDipendente(String tipo){
        if(tipo.equals("cameriere")){
            return new Cassiere();
        }
        if(tipo.equals("cassiere")){
            return new Cassiere();
        }
        if(tipo.equals("cuoco")){
            return new Cassiere();
        }

        throw new RuntimeException("Unhandled dipendente");
    }
}
