package com.company.ristorante.gestione;

import com.company.ristorante.ambiente.Tavolo;
import com.company.ristorante.persone.Cliente;

import java.util.Date;

/** MISSION
 * Questa classe provvede un ADT per la prenotazione
 */
public class Prenotazione extends ElencoPrenotazioni {

    private Integer id;
    private Tavolo tavolo;
    private Cliente nomeCliente;
    private Date data;


    /**
     * Costruttore della prenotazione
     */
    Prenotazione(Integer i, Tavolo t, Cliente c, Date d){
        this.id = i;
        this.tavolo = t;
        this.nomeCliente = c;
        this.data = d;
    }

    public Integer getId() {
        return id;
    }
}
