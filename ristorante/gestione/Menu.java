package com.company.ristorante.gestione;

import com.company.ristorante.alimenti.Alimento;

import java.util.Vector;

/** MISSION
 * This class provides an ADT for sets of int.
 * UnorderedIntSet is mutable, unbounded.
 * Questa classe provvede un ADT per i tavoli
 *
 */
public class Menu {
    /**
     * FUNZIONE DI ASTRAZIONE: la sala e rapresentata dagli elemente in this.tavoli
     * i tavoli non sono ordinati
     * INVARIANTE tavoli != null & tavoli non contiene duplicati
     */

    private Vector<Alimento> alimenti;

    /**
     * Costruttore
     * EFFETTO; iniziallizza un nuovo menu
     */
    Menu(){
        this.alimenti = new Vector<Alimento>();
    }
}
