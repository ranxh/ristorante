package com.company.ristorante.gestione;

import java.util.*;

/**
 * MISSIONE
 * Questa classe provvede un ADT per le prenotazioni
 * ElencoPrenotazioni e mutabile, unbounded
 */
public class ElencoPrenotazioni {

    private Vector<Prenotazione> prenotazioni;

    /**
     * Costruttore
     */
    ElencoPrenotazioni(){
        this.prenotazioni = new Vector<Prenotazione>();
    }

    ElencoPrenotazioni(ElencoPrenotazioni e){
        this.prenotazioni = (Vector<Prenotazione>) e.prenotazioni.clone();
    }

    /**
     * Crea una copia ordinata rispetto l'id delle prenotazioni
     * @return
     */
    public ElencoPrenotazioni sortedCopy(){
        ElencoPrenotazioni copy = new ElencoPrenotazioni(this);
        copy.sort();
        return copy;
    }

    /**
     * Creo un ordinazione utilizzando una classe anonima
     */
    private void sort(){
        Collections.sort(this.prenotazioni, new Comparator<Prenotazione>() {
            @Override
            public int compare(Prenotazione p1, Prenotazione p2) {
                return p1.getId() - p2.getId();
            }
        });
    }

    // testo con le lambda invece
//    private void sort(){
//        Collections.sort(this.prenotazioni, (p1, p2) -> p1.getId() - p2.getId());
//    }




    /** ====================================
     * CLASSE INTERNA per l'iteratore
     */
    private class ElencoPrenotazioniIterator implements Iterator<Prenotazione> {

        /**
         * INVARIANTE
         * quando questo iteratore viene creato dipendenti tiene la coppia dei
         * dipendinti, current e un integer i cosi che listaPersonale[i] e il primo
         * dipendente non ancora tornato
         * current==dipendenti.size() abbiamo esplorato tutta la lista
         */

        private int current;
        final private Vector<Prenotazione> prenotazioni;

        ElencoPrenotazioniIterator(ElencoPrenotazioni p){
            this.prenotazioni = (Vector<Prenotazione>) p.prenotazioni.clone();
            this.current = 0;
        }

        @Override
        public boolean hasNext(){
            return this.current < prenotazioni.size();
        }

        @Override
        public Prenotazione next(){
            if (this.current < prenotazioni.size()){
                Prenotazione res = this.prenotazioni.get(this.current);
                this.current++;
                return(res);
            }
            throw new NoSuchElementException("Went beyond the available values");
        }
    }
}
