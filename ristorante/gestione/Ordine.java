package com.company.ristorante.gestione;

import com.company.ristorante.alimenti.Alimento;
import com.company.ristorante.ambiente.Tavolo;
import com.company.ristorante.persone.Cliente;

import java.util.Vector;

public class Ordine {

    private Vector<Alimento> listaAlimenti;
    private Cliente nomeOrdinante;
    private Tavolo tavoloOrdinante;

    /**
     * Costruttore
     */
    Ordine(Vector<Alimento> l, Cliente n, Tavolo t){
        this.listaAlimenti = l;
        this.nomeOrdinante = n;
        this.tavoloOrdinante = t;
    }

}
