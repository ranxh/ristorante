package com.company.ristorante.alimenti;

public class AggiungiAlimento {
    public static void main(String a[]){
        /**
         * Uso la factory per nascondere i dettagli relativi alla costruzione
         * degli oggeti; nel nostro caso per aggiungere alimenti
         */
        try {
            AlimentoFactory alimento = new AlimentoFactory();
            Alimento al = alimento.createAlimento("caffe");
            System.out.format("Alimento: %s", al.getName());
        }
        catch (AlimentoNotFoundException e){
            System.out.println ("Errore alimento ");
        }
    }
}
