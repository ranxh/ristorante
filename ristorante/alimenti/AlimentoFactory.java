package com.company.ristorante.alimenti;

public class AlimentoFactory {
    public Alimento createAlimento(String tipo) throws AlimentoNotFoundException {
        if (tipo.equals("caffe")){
            return new Caffe();
        }
        throw new AlimentoNotFoundException("Alimento non esistente");
    }
}