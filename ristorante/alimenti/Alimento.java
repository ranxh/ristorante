package com.company.ristorante.alimenti;

public interface Alimento {

    public String getName();
}