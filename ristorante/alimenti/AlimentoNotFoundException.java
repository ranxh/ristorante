package com.company.ristorante.alimenti;

public class AlimentoNotFoundException extends Exception {

    public AlimentoNotFoundException() {
        super("Alimento non trovato Exception");
    }
    public AlimentoNotFoundException(String s) {
        super(s);
    }
}
