package com.company.ristorante.alimenti;

public class Caffe implements Alimento {
    private String nome;

    Caffe(){
        this.nome = "Caffe";
    }

    @Override
    public String getName() {
        return this.nome;
    }
}
