package com.company.ristorante.ambiente;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Vector;

/** MISSIONE
 * Questa classe provvede un ADT per i tavoli
 *
 */
public class Sala {
    /**
     * FUNZIONE DI ASTRAZIONE: la sala e rapresentata dagli elemente in this.tavoli
     * i tavoli non sono ordinati
     * INVARIANTE tavoli != null & tavoli non contiene duplicati
     */
    private Vector<Tavolo> tavoli;

    /**
     * costruttore
     * EFFETTO; initializza a una nuova sala, vuota
     */
    Sala(){
        this.tavoli = new Vector<Tavolo>();
    }


    /**
     * MODIFICA il vettore dei tavoli
     * @param t il tavolo da aggiungere : REQUIRE not null
     * @return True se va a buon fine
     */
    public boolean aggiungeTavolo(Tavolo t){
        this.tavoli.add(t);
        return true; //TODO
    }

//    public Tavolo cercaLibero(Integer nPosti){}

    /** =======================================
     * CLASSE INTERNA fer un iteratore generico
     * =======================================
     */

    /**
     * MISSION provedere un iteratore sui tavoli della sala
     * quando l'iteratore e creato se il set
     * cambia l'iteratore lavora sui dati originali
     */
    private class SalaIterator implements Iterator<Tavolo> {

        /**
         * INVARIANTE
         * quando questo iteratore viene  creato tavoli contiene la coppia dei tavoli della sala
         * current e l'integer i cosi che tavoli[i] e il primo tavolo non ancora tornato
         * current==tavoli.size quando esploriamo tutti gli elementi
         */
        private int current;
        final private Vector<Tavolo> tavoli;

        /**
         * @param s e una Sala; REQUIRE not null
         *        Inizializza l'iteratore con la size del vettore e current=0,
         *        salva pure una copia dei tavoli.
         */
        SalaIterator(Sala s) {
            Objects.requireNonNull(s);
            this.tavoli = (Vector<Tavolo>) s.tavoli.clone();
            this.current = 0;
        }

        @Override
        public boolean hasNext() {
            return (this.current < this.tavoli.size());
        }

        @Override
        public Tavolo next() {
            if (this.current < this.tavoli.size()) {
                Tavolo res = this.tavoli.get((int) this.current);
                this.current++;
                return (res);
            } else {
                throw new NoSuchElementException("Went beyond the available values");
            }
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }
    }
}
