package com.company.ristorante.ambiente;

import com.company.ristorante.persone.Cliente;

import java.util.Vector;

public class Tavolo extends Sala{
    private Integer Id;
    private Integer numeroPosti;
    private Vector<Cliente> seduti;

    /**
     * Costruttore
     */
    Tavolo(Integer i, Integer n){
        this.Id = i;
        this.numeroPosti = n;
        this.seduti = new Vector<Cliente>();
    }

    /**
     * Controlla se e libero o no
     * @return
     */
    public boolean getSeduti() {
        return seduti.isEmpty();
    }
}
